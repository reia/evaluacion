import React from 'react';
import logo from './logo.svg';
import './App.css';
import Lista from './Lista';
import Palabras from './Palabras';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      palabras: this.props.palabras,
      palabra: this.props.palabra,
    }   

    this.nuevaPalabra = this.nuevaPalabra.bind(this);
  }

  nuevaPalabra(event){
    this.setState({
        palabra: event.target.value,
    })

  this.state.palabras.push(this.state.palabra);
} 

render(){

  return(
            
  <div>
  <Palabras />
  <Lista />
  </div>
  
 );
  

}
};





