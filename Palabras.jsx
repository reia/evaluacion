import React from 'react';
import App from './App';
import {Button} from 'reactstrap';
export default class Palabra extends React.Component{
    
    render(){
        return(
        <>
        <input type="text" 
                name="nombre" 
                id="nombreInput"
                value={this.props.palabra}></input>
        <Button outline color="success" onClick={this.nuevaPalabra}>Afegir</Button>
        </>
        );
    }
}