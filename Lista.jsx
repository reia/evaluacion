import React from 'react';
import App from './App';


export default class Lista extends React.Component{
    
        
    render() {
        
        let lista = this.props.palabras.sort((a,b) => a.id-b.id).map(el => {
            return (
                <li>{el.palabras}</li>
            );
        })


        return (
            <ul>
            {lista}
            </ul>
        );
    }


}

